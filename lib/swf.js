var Promise = require("node-promise").Promise;
aws = require("./aws");

exports.createClient = function (accessKeyId, secretAccessKey, options) {
  options = options || {};

  var client = aws.awsClient({
    host: options.host || "swf.us-east-1.amazonaws.com",
    path: "/",
    accessKeyId: accessKeyId,
    secretAccessKey: secretAccessKey,
    secure: true
  });
  return {
    getDomains: function (deprecated) {
      var promise = new Promise();
      client.call("ListDomains", {
        registrationStatus: deprecated ? "DEPRECATED" : "REGISTERED"
      }, function (result) {
        promise.resolve(result.domainInfos);
      });
      return promise;
    },
    getActivityTypes: function (domain, deprecated) {
      var promise = new Promise();
      client.call("ListActivityTypes", {
        domain: domain,
        registrationStatus: deprecated ? "DEPRECATED" : "REGISTERED"
      }, function (result) {
        promise.resolve(result.typeInfos);
      });
      return promise;
    },
    getWorkflowTypes: function (domain, deprecated) {
      var promise = new Promise();
      client.call("ListWorkflowTypes", {
        domain: domain,
        registrationStatus: deprecated ? "DEPRECATED" : "REGISTERED"
      }, function (result) {
        promise.resolve(result.typeInfos);
      });
      return promise;
    }
  };
};
