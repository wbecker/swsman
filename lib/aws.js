var http = require("http");
var https = require("https");
var crypto = require("crypto")

var method = "POST";

exports.awsClient = function(obj) {
  if (null == obj.secure) {
    obj.secure = true;
  }

  obj.connection = obj.secure ? https : http;
  obj.call = function (action, query, callback) {
    if (obj.secretAccessKey == null || obj.accessKeyId == null) {
      throw("secretAccessKey and accessKeyId must be set")
    }
    var now = new Date().toUTCString();
    var body = JSON.stringify(query);
    var headers = {
      "Host": obj.host,
      "Content-Type": "application/json; charset=UTF-8",
      "Content-Length": body.length,
      "Content-Encoding": "amz-1.0",
      "X-Amz-Target": "SimpleWorkflowService." + action,
      "X-Amz-Date": now
    };
    headers["X-Amzn-Authorization"] = createAuthorisation(headers, obj, body, now, action)

    var options = {
      host: obj.host,
      path: obj.path,
      method: method,
      headers: headers
    };
    var req = obj.connection.request(options, function (res) {
      var data = '';
      res.addListener('data', function (chunk) {
        data += chunk.toString()
      })
      res.addListener('end', function() {
        callback(JSON.parse(data));
      })
    });
    req.write(body)
    req.end()
  }
  return obj;
}
function createAuthorisation(headers, obj, body, now, action) {
  var keys = []
  for(var key in headers) {
    var pre = key.substring(0, 5);
    if ((pre === "Host") || (pre === "X-Amz")) {
      keys.push(key);
    }
  }
  keys = keys.sort()
  return [
    "AWS3 ",
    "AWSAccessKeyId=" + obj.accessKeyId + ",",
    "Algorithm=HmacSHA256,",
    "SignedHeaders=Host;X-Amz-Date;X-Amz-Target,",
    "Signature=" + sign(
      obj.path,
      keys,
      headers,
      body,
      obj.secretAccessKey)
  ].join("");
}
function sign(path, keys, headers, body, secretKey) {
  var content;
  content = keys.map(function (key) {
    return (key.toLowerCase()+":"+headers[key]).trim();
  });  
  var stringToSign = [method, path, "", content.join("\n"), "", body.trim()].join("\n");
  // Amazon signature algorithm seems to require this
  stringToSign = stringToSign.replace(/'/g,"%27");
  stringToSign = stringToSign.replace(/\*/g,"%2A");
  stringToSign = stringToSign.replace(/\(/g,"%28");
  stringToSign = stringToSign.replace(/\)/g,"%29");
  return hmacSha256(secretKey, sha256(stringToSign));
}
function sha256(toHash) {
  return crypto.createHash("sha256").update(toHash).digest();
}
function hmacSha256(key, toSign) {
  var hash = crypto.createHmac("sha256", key);
  return hash.update(toSign).digest("base64");
}

