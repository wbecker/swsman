var u = require("underscore"),
    fs = require("fs"),
    express = require("express"),
    config = require("config"),
    swf = require("./lib/swf");

var createServer = function () {
  server = express.createServer();
  server.configure(function () {
    server.use(express["static"]("client"));
  });
  server.listen(8001);
  server.use(express.bodyParser());
  server.get("/api/0.1/domain", getDomains);
  server.get("/api/0.1/domain/:name/activity_type", getActivityTypes);
  server.get("/api/0.1/domain/:name/workflow_type", getWorkflowTypes);
  server.get("/api/0.1/action", getActions);
  server.post("/api/0.1/action", addAction);
  server.put("/api/0.1/action/:id", updateAction);
  server.get("/api/0.1/connection", getConnection);
  server.post("/api/0.1/connection", addConnection);
  server.put("/api/0.1/connection/:id", updateConnection);
};
var getDomains = function (req, res) {
  client.getDomains().then(function (result) {
    res.write(JSON.stringify(result));
    res.end();
  });
}
var getActivityTypes = function (req, res) {
  client.getActivityTypes(req.params.name).then(function (result) {
    res.write(JSON.stringify(result));
    res.end();
  });
}
var getWorkflowTypes = function (req, res) {
  client.getWorkflowTypes(req.params.name).then(function (result) {
    res.write(JSON.stringify(result));
    res.end();
  });
}
var actions = [];
var connections = [];
var getActions = function (req, res) {
  res.write(JSON.stringify(actions));
  res.end();
};
var addAction = function (req, res) {
  var action = req.body;
  action.id = actions.length + 1;
  actions.push(action);
  res.write(JSON.stringify(action));
  res.end();
};
var updateAction = function (req, res) {
  actions[req.params.id - 1] = req.body;
  res.end();
};
var getConnection = function (req, res) {
  res.write(JSON.stringify(connections));
  res.end();
};
var addConnection = function (req, res) {
  connections.push(req.body);
  res.end();
};
var updateConnection = function (req, res) {
  connections[req.params.id - 1] = req.body;
  res.end();
};
var client = swf.createClient(config.aws.accessKeyId, config.aws.secretAccessKey, {});
createServer();
