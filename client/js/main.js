require.config({
  paths: {
    jQuery: "libs/jquery/jquery",
    Underscore: "libs/underscore/underscore",
    Backbone: "libs/backbone/backbone",
    draw2d: "libs/draw2d/draw2d",
    templates: "../templates"
  }
});
require([
  "swsman",
  "order!libs/jquery/jquery-1.7.1-min",
  "order!libs/underscore/underscore-1.3.1-min",
  "order!libs/backbone/backbone-0.9.1-min",
  "order!libs/draw2d/draw2d-0.9.26",
], function(swsman){
  swsman.initialize();
});
