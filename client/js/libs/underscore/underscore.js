define(['order!libs/underscore/underscore-1.3.1-min'], function(){
  _.templateSettings = {
    interpolate : /\{\{(.+?)\}\}/g
  };
  return _;
});
