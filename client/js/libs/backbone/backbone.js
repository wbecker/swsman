define([
    'order!libs/backbone/backbone-0.9.1-min'
  ], function () {
  _.noConflict();
  $.noConflict();
  return Backbone.noConflict();
});
