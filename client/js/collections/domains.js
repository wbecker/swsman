define([
  'jQuery',
  'Underscore',
  'Backbone',
  'models/domain',
], function($, _, Backbone, DomainModel){
  return new (Backbone.Collection.extend({
    model: DomainModel,
    url: "api/0.1/domain",
    initialize: function () {
    }
  }));
});
