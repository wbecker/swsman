define([
  'jQuery',
  'Underscore',
  'Backbone',
  'models/connection',
], function($, _, Backbone, ConnectionModel){
  return new (Backbone.Collection.extend({
    model: ConnectionModel,
    url: "api/0.1/connection",
    initialize: function () {
    }
  }));
});

