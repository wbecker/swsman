define([
  'jQuery',
  'Underscore',
  'Backbone',
  'models/workflowType',
], function($, _, Backbone, WorkflowTypeModel){
  return function (domain) {
    return new (Backbone.Collection.extend({
      model: WorkflowTypeModel,
      url: "api/0.1/domain/" + domain.get("name") + "/workflow_type",
      initialize: function () {
        this.bind("reset", this.addParentToAll, this); 
      },
      addParentToAll: function (workflowType) {
        this.each(this.addParent);
      },
      addParent: function (workflowType) {
        workflowType.set("domain", domain.toJSON());
      }
      
    }));
  };
});


