define([
  'jQuery',
  'Underscore',
  'Backbone',
  'models/action',
], function($, _, Backbone, ActionModel){
  return new (Backbone.Collection.extend({
    model: ActionModel,
    url: "api/0.1/action",
    initialize: function () {
    }
  }));
});
