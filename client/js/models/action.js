define([
  "jQuery",
  "Underscore",
  "Backbone"
], function($, _, Backbone, Connections){
  return Backbone.Model.extend({
    defaults: {
      name: "New Action",
      type: "Action",
      x: 200,
      y: 200
    },
    initialize: function () {
    }
  });
});

