define([
  "jQuery",
  "Underscore",
  "Backbone"
], function($, _, Backbone){
  return Backbone.Model.extend({
    defaults: {
      name: "New Domain",
    },
    initialize: function () {
    }
  });
});

