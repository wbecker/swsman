define([
  "jQuery",
  "Underscore",
  "Backbone",
  "collections/workflowTypes",
  "views/WorkflowType",
  "text!templates/domain.html"
], function($, _, Backbone, WorkflowTypeController, WorkflowTypeView, template) {
  return function () {
    return (Backbone.View.extend({
      tagName: "div",
      attributes: {
        class: "domain"
      },
      template: _.template($(template).html()),
      events: {
        "click .name": "viewWorkflows"
      },
      initialize: function () {
      },
      render: function () {
        $(this.el).html(this.template(this.model.toJSON()));
      },
      viewWorkflows: function () {
        Backbone.history.navigate("/domain/" + this.model.get("name"));
        this.getWorkflowTypeController().fetch();   
      },
      getWorkflowTypeController: function () {
        if (!this.workflowTypeController) {
          this.workflowTypeController = WorkflowTypeController(this.model);
          this.workflowTypeController.bind("reset", this.addWorkflowTypes, this);
        }
        return this.workflowTypeController;
      },
      addWorkflowTypes: function () {
        this.getWorkflowTypeController().each(this.addWorkflowType, this);
      },
      addWorkflowType: function (workflowType) {
        var view = new WorkflowTypeView({model: workflowType});
        view.render();
        $(this.el).find(".workflowTypes").append(view.el);
      }
    }));
  }();
});
 
