define([
  "draw2d",
  "views/Action"
], function(draw2d, ActionView) {
  var End = function () {
    this.init.apply(this, arguments);
  };
  End.prototype = new ActionView;
  End.prototype.type = "End";
  End.prototype.outputPort = undefined;
  return End;
});
