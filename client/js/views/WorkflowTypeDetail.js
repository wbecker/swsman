define([
  "jQuery",
  "Underscore",
  "Backbone",
  "draw2d",
  "collections/actions",
  "collections/connections",
  "views/Start",
  "views/End",
  "views/Action",
  "views/ActionDetail",
  "text!templates/WorkflowTypeDetail.html"
], function($, _, Backbone, draw2d, Actions, Connections, 
    StartView, EndView, ActionView, ActionDetailView, template) {
  return function () {
    return new (Backbone.View.extend({
      el: $("body"),
      events: {
        "click .add_action": "addNewAction"
      },
      initialize: function () {
        this.actionViews = {};
        this.workflowReady = $.Deferred();
        Actions.bind("reset", this.addAllActions, this);
        Actions.bind("add", this.addAction, this);
        Connections.bind("reset", this.addAllConnections, this);
        Connections.bind("add", this.addConnection, this);
        Actions.fetch();
      },
      render: function () {
        $(this.el).html(template);
        setTimeout(_.bind(function () {
          this.getWorkflow();
        }, this), 1);
      },
      addAllActions: function () {
        $.when(this.workflowReady).then(_.bind(function () { 
          Actions.each(this.addAction, this);
          if (Actions.size() === 0) {
            this.addStart();
            this.addEnd();
          }
          Connections.fetch();
        }, this));
      },
      addAction: function (action) {
        var viewType;
        switch(action.get("type")) {
        case "Start":
          viewType = StartView;
          break
        case "End":
          viewType = EndView;
          break
        default:
          viewType = ActionView
        }
        var view = new viewType({
          model: action, 
          onDrag: _.bind(this.handleDrag),
          onConnectionAdded: _.bind(this.connectionAdded, this),
          onDoubleClick: _.bind(function (model) {
            var view = new ActionDetailView({model: model});
            view.render();
            $(this.el).append(view.el);
          }, this)
        });
        view.render(this.getWorkflow());
        action.bind("sync", view.update, view);
        this.registerView(action, view);
      },
      registerView: function (action, view) {
        if (action.id) {
          this.actionViews[action.id] = view;
        } else {
          setTimeout(_.bind(this.registerView, this, action, view), 100);
        }
      },
      connectionAdded: function (input, output) {
        console.log(input.id, output.id);
        Connections.create({
          from: input.id,
          to: output.id
        });
      },
      addAllConnections: function () {
        Connections.each(this.addConnection, this);
      },
      addConnection: function (connection) {
        var c = new draw2d.Connection();
        c.setSource(this.actionViews[connection.get("from")].outputPort);
        c.setTarget(this.actionViews[connection.get("to")].inputPort);
        this.getWorkflow().addFigure(c);

      },
      handleDrag: function (view, model) {
        model.set("x", view.getAbsoluteX());
        model.set("y", view.getAbsoluteY());
        model.save();
        view.update(view.getAbsoluteX(), view.getAbsoluteY());
      },
      addStart: function () {
        Actions.create({
          type: "Start",
          x: 200,
          y: 100
        });
      },
      addEnd: function () {
        Actions.create({
          type: "End",
          x: 400,
          y: 100
        });
      },
      addNewAction: function () {
        Actions.create({});
      },
      getWorkflow: function () {
        if (!this.workflow) {
          draw2d.Connection.setDefaultRouter(new draw2d.NullConnectionRouter());
          this.workflow = new draw2d.Workflow("paintarea");
          this.workflowReady.resolve(this.workflow);
        }
        return this.workflow;
      }
    }));
  };
});
