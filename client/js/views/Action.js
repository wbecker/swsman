define([
  "Underscore",
  "draw2d",
], function(_, draw2d) {
  var Action = function () {
    this.init.apply(this, arguments);
  };
 
  Action.prototype = new draw2d.ImageFigure();
  Action.prototype.init = function (opts) {
    draw2d.ImageFigure.call(this,"/img/"+this.type+".png");
    this.setDimension(50,50);
    if (opts) {
      this.model = opts.model;
      this.dragendHandler = opts.onDrag;
      this.onConnectionAdded = opts.onConnectionAdded;
      this.onDoubleClickHandler = opts.onDoubleClick;
      this.text = new draw2d.Label();
      this.text.setText(this.model.get("name"));
    }
  };
  Action.prototype.type = "End";
  Action.prototype.inputPort = null;
  Action.prototype.outputPort = null;

  var original_onDragend = Action.prototype.onDragend;
  Action.prototype.onDragend = function () {
    original_onDragend.apply(this, arguments);
    this.dragendHandler(this, this.model);
  };
  Action.prototype.onDoubleClick = function () {
    this.onDoubleClickHandler(this.model);
  };
  Action.prototype.render = function (workflow) {
    workflow.addFigure(this, this.model.get("x"), this.model.get("y"));
    workflow.addFigure(this.text, this.model.get("x"), this.model.get("y") + 60);
  };
  Action.prototype.update = function () {
    this.text.setPosition(this.model.get("x"), this.model.get("y") + 60);
    this.text.setText(this.model.get("name"));
  }
  
  Action.prototype.setWorkflow = function (/*:draw2d.Workflow*/ workflow) {
    var input, output, inputOnDrop, outputOnDrop;
    draw2d.ImageFigure.prototype.setWorkflow.call(this,workflow);
  
    if ((workflow !== null) && (this.inputPort===null)) {
      input = new draw2d.InputPort();
      input.setWorkflow(workflow);
      input.setBackgroundColor(new draw2d.Color(115,115,245));
      inputOnDrop = input.onDrop;
      input.onDrop = _.bind(function (outputPort) {
        if (inputOnDrop) {
          inputOnDrop.apply(input, arguments);
        }
        this.onConnectionAdded(outputPort.parentNode.model, this.model);
      }, this);
      this.addPort(input, 0, this.height/2);
      this.inputPort = input;
    }

    if ((workflow !== null) && (this.outputPort === null)) {
      output= new draw2d.OutputPort();
      output.setMaxFanOut(5); // It is possible to add "5" Connector to this port
      output.setWorkflow(workflow);
      output.setBackgroundColor(new  draw2d.Color(245,115,115));
      outputOnDrop = output.onDrop;
      output.onDrop = _.bind(function (inputPort) {
        if (outputOnDrop) {
          outputOnDrop.apply(output, arguments);
        }
        this.onConnectionAdded(this.model, inputPort.parentNode.model);
      }, this);
      this.addPort(output, this.width, this.height/2);
      this.outputPort = output;
    }
  };
  return Action;
});
