define([
  "jQuery",
  "Underscore",
  "Backbone",
  "views/WorkflowTypeDetail",
  "text!templates/workflowType.html"
], function($, _, Backbone, WorkflowTypeDetailView, template) {
  return function () {
    return (Backbone.View.extend({
      tagName: "li",
      attributes: {
        class: "workflowType"
      },
      template: _.template($(template).html()),
      events: {
        "click .name": "showDetail"
      },
      initialize: function () {
      },
      render: function () {
        $(this.el).html(this.template(this.model.toJSON()));
      },
      showDetail: function () {
        Backbone.history.navigate("domain/" + this.model.get("domain").name + 
          "/workflow_type/" + this.model.get("workflowType").name);
        var view = new WorkflowTypeDetailView({model: this.model});
        view.render();
        $(this.el).find(".workflowTypes").append(view.el);
      }
    }));
  }();
});
 
