define([
  "draw2d",
  "views/Action"
], function(draw2d, ActionView) {
  var Start = function () {
    this.init.apply(this, arguments);
  };
  Start.prototype = new ActionView;
  Start.prototype.type = "Start";
  Start.prototype.inputPort = undefined;
  return Start;
});
