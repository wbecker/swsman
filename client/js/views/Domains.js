define([
  "jQuery",
  "Underscore",
  "Backbone",
  "collections/domains",
  "views/Domain",
  "text!templates/domains.html"
], function($, _, Backbone, Domains, DomainView, template) {
  return function () {
    return new (Backbone.View.extend({
      el: $("body"),
      events: {
        //"click .add_action": "addNewAction"
      },
      initialize: function () {
        Domains.bind("reset", this.addAllDomains, this);
        Domains.bind("add", this.addDomain, this);
        Domains.fetch();
      },
      render: function () {
        $(this.el).html(template);
      },
      addAllDomains: function () {
        Domains.each(this.addDomain, this);
      },
      addDomain: function (domain) {
        var view = new DomainView({model: domain});
        view.render();
        $(this.el).find(".domains").append(view.el);
      }
    }))
  }();
});
 
