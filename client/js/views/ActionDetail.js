define([
  "jQuery",
  "Underscore",
  "Backbone",
  "text!templates/ActionDetail.html"
], function($, _, Backbone, template) {
  return function () {
    return (Backbone.View.extend({
      tagName: "div",
      template: _.template($(template).html()),
      events: {
        "click .save": "save",
        "click .close": "close"
      },
      initialize: function () {
      },
      render: function () {
        $(this.el).html(this.template(this.model.toJSON()));
      },
      save: function () {
        this.model.set("name", this.$el.find(".name").val());
        this.model.save();
        this.close();
      },
      close: function () {
        this.$el.detach();
      }
    }));
  }();
});
