define([
  "jQuery",
  "Underscore",
  "Backbone",
  "views/home/main",
  "views/Domains"
], function($, _, Backbone, mainHomeView, domainListView){
  var AppRouter = Backbone.Router.extend({
    initialize: function () {
      this.views = {
        mainHomeView: mainHomeView()
      };
    },
    routes: {
      "domain": "showDomains",
      "*actions": "defaultAction"
    },
    showDomains: function(){
      domainListView.render();
    },
    defaultAction: function(actions){
      this.views.mainHomeView.render(); 
    }
  });

  var initialize = function(){
    var app_router = new AppRouter;
    Backbone.history.start();
  };
  return { 
    initialize: initialize
  };
});
